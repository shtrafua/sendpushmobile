﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

using System.Threading;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Xml;
using Google.Apis.Auth.OAuth2;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
namespace SendPush
{
    class Program
    {

        string SlackPushURL = ConfigurationManager.ConnectionStrings["SlackPushURL"].ConnectionString;
        static void Main(string[] args)
        {

            Console.WriteLine("-------");


            var sbOrderJson = new StringBuilder();

            string MessageTitlePush = "";
            string MessageTextPush = "";
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            var listOfCustomer = new List<ReaderInfo>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                listOfCustomer = GetData("SELECT id, Email, DeviceID, DeviceType from Customer where Deleted = 0 and DeviceID is not null and DeviceID != '' and id = 14136", connection);
                connection.Close();
            }
            
            MessageTitlePush = "Test title";
            MessageTextPush = "Test message";
            var readerList = new List<ReaderInfo>();
            string token = GetAccessToken();



            foreach (var row in listOfCustomer)
            {
                if (row.DeviceID != "")
                {
                        sbOrderJson.AppendLine("{\"message\" :{\"token\" :\"" + row.DeviceID + "\"," +
                            "\"notification\" : " +
                                "{\"body\" : \"" + String.Format(MessageTextPush) + "\"," +
                                "\"title\" : \"" + MessageTitlePush + "\"}, " +
                                "\"data\":{" +
                                "\"screen\" : \"" + "Список полисов" + "\" }" +
                            " }}");
                    var inputRequset = new HttpRequestMessage
                    {
                        Content = new StringContent(sbOrderJson.ToString(), Encoding.UTF8, "application/json")
                    };

                    var sendPushClient = new HttpClient();
                    sendPushClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = sendPushClient.PostAsync("https://fcm.googleapis.com/v1/projects/shtrafua-bd62e/messages:send", inputRequset.Content).Result;
                    SendSlackPush("pushservice", "pushBot", "Message: " + MessageTitlePush + " | Message text: " + MessageTextPush + " | DeviceID:" + row.DeviceID + " | Email:" + row.Email + " | DeviceType:" + row.DeviceType + " | Id:" + row.Id + " | response.StatusCode: "+ response.StatusCode.ToString(), ":dash:");
                    Console.WriteLine(DateTime.Now.ToString() + " | Id:" + row.Id + " | response.StatusCode: " + response.StatusCode.ToString());
                    Thread.Sleep(10); //delay between messages 10 = 1.5h
                }
            }

            
            Console.ReadLine();
        }

        private static string GetAccessToken()
        {
            using (var stream = new FileStream(ConfigurationManager.AppSettings["KeyPass"], FileMode.Open, FileAccess.Read))
            {
                var credential = GoogleCredential.FromStream(stream).CreateScoped("https://www.googleapis.com/auth/firebase.messaging");
                return credential.UnderlyingCredential.GetAccessTokenForRequestAsync().Result;
            }
        }

        static List<ReaderInfo> GetRows(SqlDataReader reader)
        {
            //var merchants = Merchants.GetMerchants().MerchantList;
            var result = new List<ReaderInfo>();
            if (reader.HasRows)
            {
                while (reader.Read()) // построчно считываем данные
                {
                    try
                    {
                        var cl = Thread.CurrentThread.CurrentCulture;
                        string idtestString = (reader["Id"].ToString());
                        int idtest = int.Parse(idtestString, System.Globalization.NumberStyles.Integer);
                        string testtest = idtest.ToString();

                        var row = new ReaderInfo
                        {
                            
                            
                            Id = int.Parse((reader["Id"].ToString()), System.Globalization.NumberStyles.Integer),
                            Email = reader["Email"].ToString(),
                            DeviceID = reader["DeviceID"].ToString(),
                            DeviceType = reader["DeviceType"].ToString()
                            
                        };
                        result.Add(row);
                    }
                    catch (Exception ex)
                    {
                        string error = "Ошибка " + ex.Message + "time:" + DateTime.Now;

                    }
                }
            }
            return result;
        }
        public static List<ReaderInfo> GetData(string commandText, SqlConnection connection)
        {
            SqlCommand command1 = new SqlCommand
            {
                CommandText = commandText,
                Connection = connection
            };
            var reader1 = command1.ExecuteReader();
            var list1 = GetRows(reader1);
            reader1.Dispose();
            return list1;
        }


        public class ReaderInfo
        {
            public int Id { get; set; }
            public string Email { get; set; }
            public string DeviceID { get; set; }
            public string DeviceType { get; set; }
        

        }

        public static bool SendSlackPush(string channel, string username, string text, string icon_emoji)
        {
            // icon_emoji - https://www.webfx.com/tools/emoji-cheat-sheet/
           string SlackPushURL = ConfigurationManager.AppSettings["SlackPushURL"];
            try
            {
                var data = new
                {
                    channel = channel,
                    username = username,
                    text = text,
                    icon_emoji = icon_emoji
                };
                string json = JsonConvert.SerializeObject(data);
                var client = new HttpClient();
                var response = client.PostAsync(
                       SlackPushURL,
                        new StringContent(json, Encoding.UTF8, "application/json"));


                return true;
            }
            catch { return false; }
        }

    }
}
